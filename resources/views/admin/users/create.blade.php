@extends('layouts.coreui')



@section('title','Admin Home')

@section("content")

<!-- http://localhost:8000/admin/users -->
<div class="container-fluid mt-5">
  <div class="row">
    <div class="col">
      <div class="card">
<div class="card-header">
<strong>Horizontal</strong> Form</div>
<div class="card-body">
<form class="form-horizontal" action="{{isset($user)?route('users.update',$user->id):route('users.store')}}" method="post">
  @csrf
  @if(isset($user))
    @method("put")
  @else
    @method("post")
  @endif
  <div class="form-group row">
     <label class="col-md-3 col-form-label" for="hf-email">Email</label>
      <div class="col-md-9">
          <input class="form-control" value="{{isset($user)? $user->name:''}}" id="hf-name" type="text" name="name" placeholder="Enter Name.." autocomplete="email">
          <span class="help-block">Please enter your name</span>
      </div>
  </div>
      <div class="form-group row">
         <label class="col-md-3 col-form-label" for="hf-email">Email</label>
          <div class="col-md-9">
              <input class="form-control" value="{{isset($user)? $user->email:''}}" id="hf-email" type="email" name="email" placeholder="Enter Email.." autocomplete="email">
              <span class="help-block">Please enter your email</span>
          </div>
      </div>
<div class="form-group row">
<label class="col-md-3 col-form-label" for="hf-password">Password</label>
<div class="col-md-9">
<input class="form-control" id="hf-password" type="password" name="password" placeholder="Enter Password.." autocomplete="current-password">
<span class="help-block">Please enter your password</span>
</div>
</div>


@foreach($roles as $role)
<div class="form-check">
  <input class="form-check-input" name="roles[]" type="checkbox" value="{{$role->id}}" id="defaultCheck{{$role->id}}" {{isset($user)?(in_array($role->id,$user->rolesIds())?'checked':''):''}} >
  <label class="form-check-label" for="defaultCheck{{$role->id}}">
    {{$role->role}}
  </label>
</div>
@endforeach


</div>
<div class="card-footer">
<button class="btn btn-sm btn-primary" type="submit">
<i class="fa fa-dot-circle-o"></i> Submit</button>
<button class="btn btn-sm btn-danger" type="reset">
<i class="fa fa-ban"></i> Reset</button>
</div>
</form>
</div>

    </div>

  </div>
</div>

@endsection
