@extends('layouts.coreui')



@section('title','Admin Home')

@section("content")

<!-- http://localhost:8000/admin/users -->
<div class="container-fluid mt-5">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-align-justify"></i> User Management
        </div>

        <div class="card-body">
          <a class="btn btn-success float-right mb-2" href="{{route('users.create')}}">Add User</a>
          <table class="table table-responsive-sm">

            <thead>
                <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Role</th>
                <th>Actions</th>
                </tr>
            </thead>

            <tbody>
              @foreach($users as $user)
                <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                @foreach ($user->roles as $role)
                  <span class="badge badge-pill badge-info">{{$role->role}}<span>
                @endforeach
              </td>
                <td>
                  <form method="post" action="{{route('users.destroy',$user->id)}}">
                    @csrf
                    @method("delete")
                  <span class="badge badge-info"><a href="{{route('users.edit',$user->id)}}"><i class="icofont-edit-alt" style="color:#f0f3f"></i></a></span>


                  <span class="badge badge-danger"><button type="submit"  onclick="return confirm('Are You Sure? Want to Delete It.');" ><i class="icofont-ui-delete"></i></button></span>
                </form>
                </td>
                </tr>
               @endforeach
              </tbody>
          </table>

            {{$users}}

        </div>
      </div>
    </div>

  </div>
</div>

@endsection
