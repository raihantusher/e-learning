
<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.11
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>CoreUI Free Bootstrap Admin Template</title>
    <!-- Icons-->
    <link rel="stylesheet" href="{{asset('css/backend/coreui-icons.min.css')}}">
    <link href="{{asset('css/backend/flag-icons.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/backend/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/backend/simple-line-icons.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/backend/pace.min.css')}}" rel="stylesheet">


  </head>
  <body class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>Login</h1>
                <p class="text-muted">Sign In to your account</p>

                <form method="POST" action="{{ route('login') }}">
                    @csrf
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="icon-user"></i>
                            </span>
                          </div>
                          <input  class="form-control @error('email') is-invalid @enderror"  id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="email" autofocus>
                        </div>
                        <div class="input-group mb-4">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="icon-lock"></i>
                            </span>
                          </div>
                          <input class="form-control @error('password') is-invalid @enderror" id="password" type="password"  name="password" required autocomplete="current-password" placeholder="password">
                        </div>
                          <div class="row">
                            <div class="col-6">
                              <button type="submit" class="btn btn-primary px-4">
                                  {{ __('Login') }}
                              </button>
                            </div>
                            <div class="col-6 text-right">
                              @if (Route::has('password.request'))
                                  <a class="btn btn-link px-0" href="{{ route('password.request') }}">
                                      {{ __('Forgot Your Password?') }}
                                  </a>
                              @endif
                            </div>
                          </div>
                        </form>
              </div>
            </div>
            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <h2>Sign up</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                   <a  class="btn btn-primary active mt-3"  href="{{ route('register') }}">{{ __('Register') }}</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="https://coreui.io/demo/vendors/jquery/js/jquery.min.js"></script>
    <script src="https://coreui.io/demo/vendors/popper.js/js/popper.min.js"></script>
    <script src="https://coreui.io/demo/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://coreui.io/demo/vendors/pace-progress/js/pace.min.js"></script>
    <script src="https://coreui.io/demo/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
    <script src="https://coreui.io/demo/vendors/@coreui/coreui-pro/js/coreui.min.js"></script>
  </body>
</html>
