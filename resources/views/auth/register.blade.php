
<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.11
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>CoreUI Free Bootstrap Admin Template</title>
    <!-- Icons-->
  
    <link rel="stylesheet" href="{{asset('css/backend/coreui-icons.min.css')}}">
    <link href="{{asset('css/backend/flag-icons.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/backend/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/backend/simple-line-icons.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/backend/pace.min.css')}}" rel="stylesheet">

  </head>


  <body class="app flex-row align-items-center">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-md-6">
                  <div class="card mx-4">
                    <div class="card-body p-4">
                        <h1>Register</h1>
                        <p class="text-muted">Create your account</p>

                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="icon-user"></i>
                            </span>
                          </div>

                          <input id="name" placeholder="Username" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        </div>

                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                          </div>
                          <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        </div>


                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                            <i class="icon-lock"></i>
                            </span>
                          </div>
                        <input id="password" placeholder="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        </div>


                        <div class="input-group mb-4">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                            <i class="icon-lock"></i>
                            </span>
                          </div>
                        <input id="password-confirm" placeholder="Repeat password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                        <button type="submit" class="btn btn-block btn-success">
                            {{ __('Register') }}
                        </button>
                      </form>
                    </div><!-- card body end-->


                  </div> <!-- card-mx-4 -->
                </div> <!-- col-md-6-->
              </div> <!-- row -->
            </div> <!-- container end-->


  <!-- CoreUI and necessary plugins-->
  <script src="https://coreui.io/demo/vendors/jquery/js/jquery.min.js"></script>
  <script src="https://coreui.io/demo/vendors/popper.js/js/popper.min.js"></script>
  <script src="https://coreui.io/demo/vendors/bootstrap/js/bootstrap.min.js"></script>
  <script src="https://coreui.io/demo/vendors/pace-progress/js/pace.min.js"></script>
  <script src="https://coreui.io/demo/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
  <script src="https://coreui.io/demo/vendors/@coreui/coreui-pro/js/coreui.min.js"></script>
</body>
</html>
