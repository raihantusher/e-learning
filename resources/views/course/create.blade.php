@extends('layouts.coreui')



@section('title','Admin Home')

@section("content")

<!-- http://localhost:8000/admin/users -->
<div class="container-fluid mt-5">
  <div class="row">
    <div class="col">
      <div class="card">
          <div class="card-header">
              <strong>Create</strong> Course</div>
              <div class="card-body">
                <form class="form-horizontal" action="" method="post">
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="hf-email">Course Code</label>
                    <div class="col-md-9">
                      <input class="form-control" id="hf-email" type="text" name="code" placeholder="Course Code.." >
                      <span class="help-block">Please enter your email</span>
                    </div>
                  </div>


                  <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="hf-email">Course Title</label>
                    <div class="col-md-9">
                      <input class="form-control" id="hf-email" type="email" name="name" placeholder="Course Name.." >
                      <span class="help-block">Please enter your email</span>
                    </div>
                  </div>



                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="description">Description</label>
                  <div class="col-md-9">

                    <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                  </div>
                </div>
              </form>
            </div><!-- card body -->


            <div class="card-footer">
            <button class="btn btn-sm btn-primary" type="submit">
            <i class="fa fa-dot-circle-o"></i> Submit</button>
            <button class="btn btn-sm btn-danger" type="reset">
            <i class="fa fa-ban"></i> Reset</button>
            </div>
</div>

    </div>

  </div>
</div>
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>

<script>
  CKEDITOR.replace( 'description' );
</script>
@endsection


@section('script')

@endsection
