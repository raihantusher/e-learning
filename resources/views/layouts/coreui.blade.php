<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <!--cript src="{{ asset('js/app.js') }}" defer></script-->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link rel="stylesheet" href="{{asset('css/icofont.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <!--link href="{{ asset('css/app.css') }}" rel="stylesheet"-->
    <!-- CoreUI CSS -->
  <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css">

  @yield("styles")

</head>
<body class="app sidebar-show aside-menu-show">
  <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>

      <a class="navbar-brand" href="#">
          <img class="navbar-brand-full" src="img/brand/logo.svg" width="89" height="25" alt="CoreUI Logo">
          <img class="navbar-brand-minimized" src="img/brand/sygnet.svg" width="30" height="30" alt="CoreUI Logo">
      </a>

      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
          <span class="navbar-toggler-icon"></span>
        </button>

        <ul class="nav navbar-nav d-md-down-none">
            <li class="nav-item px-3">
              <a class="nav-link" href="#">Dashboard</a>
            </li>

            <li class="nav-item px-3">
              <a class="nav-link" href="#">Users</a>
            </li>

            <li class="nav-item px-3">
              <a class="nav-link" href="#">Settings</a>
            </li>
        </ul>
<ul class="nav navbar-nav ml-auto">
<li class="nav-item dropdown d-md-down-none">
<a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
<i class="icon-bell"></i>
<span class="badge badge-pill badge-danger">5</span>
</a>
<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
<div class="dropdown-header text-center">
<strong>You have 5 notifications</strong>
</div>
<a class="dropdown-item" href="#">
<i class="icon-user-follow text-success"></i> New user registered</a>
<a class="dropdown-item" href="#">
<i class="icon-user-unfollow text-danger"></i> User deleted</a>
<a class="dropdown-item" href="#">
<i class="icon-chart text-info"></i> Sales report is ready</a>
<a class="dropdown-item" href="#">
<i class="icon-basket-loaded text-primary"></i> New client</a>
<a class="dropdown-item" href="#">
<i class="icon-speedometer text-warning"></i> Server overloaded</a>
<div class="dropdown-header text-center">
<strong>Server</strong>
</div>
<a class="dropdown-item" href="#">
<div class="text-uppercase mb-1">
<small>
<b>CPU Usage</b>
</small>
</div>
<span class="progress progress-xs">
<div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</span>
<small class="text-muted">348 Processes. 1/4 Cores.</small>
</a>
<a class="dropdown-item" href="#">
<div class="text-uppercase mb-1">
<small>
<b>Memory Usage</b>
</small>
</div>
<span class="progress progress-xs">
<div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
</span>
<small class="text-muted">11444GB/16384MB</small>
</a>
<a class="dropdown-item" href="#">
<div class="text-uppercase mb-1">
<small>
<b>SSD 1 Usage</b>
</small>
</div>
<span class="progress progress-xs">
<div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
</span>
<small class="text-muted">243GB/256GB</small>
</a>
</div>
</li>
<li class="nav-item dropdown d-md-down-none">
<a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
<i class="icon-list"></i>
<span class="badge badge-pill badge-warning">15</span>
</a>
<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
<div class="dropdown-header text-center">
<strong>You have 5 pending tasks</strong>
</div>
<a class="dropdown-item" href="#">
<div class="small mb-1">Upgrade NPM &amp; Bower
<span class="float-right">
<strong>0%</strong>
</span>
</div>
<span class="progress progress-xs">
<div class="progress-bar bg-info" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
</span>
</a>
<a class="dropdown-item" href="#">
<div class="small mb-1">ReactJS Version
<span class="float-right">
<strong>25%</strong>
</span>
</div>
<span class="progress progress-xs">
<div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</span>
</a>
<a class="dropdown-item" href="#">
<div class="small mb-1">VueJS Version
<span class="float-right">
<strong>50%</strong>
</span>
</div>
<span class="progress progress-xs">
<div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
</span>
</a>
<a class="dropdown-item" href="#">
<div class="small mb-1">Add new layouts
<span class="float-right">
<strong>75%</strong>
</span>
</div>
<span class="progress progress-xs">
<div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
</span>
</a>
<a class="dropdown-item" href="#">
<div class="small mb-1">Angular 2 Cli Version
<span class="float-right">
<strong>100%</strong>
</span>
</div>
<span class="progress progress-xs">
<div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
</span>
</a>
<a class="dropdown-item text-center" href="#">
<strong>View all tasks</strong>
</a>
</div>
</li>
<li class="nav-item dropdown d-md-down-none">
<a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
<i class="icon-envelope-letter"></i>
<span class="badge badge-pill badge-info">7</span>
</a>
<div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
<div class="dropdown-header text-center">
<strong>You have 4 messages</strong>
</div>
<a class="dropdown-item" href="#">
<div class="message">
<div class="py-3 mr-3 float-left">
<div class="avatar">
<img class="img-avatar" src="img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
<span class="avatar-status badge-success"></span>
</div>
</div>
<div>
<small class="text-muted">John Doe</small>
<small class="text-muted float-right mt-1">Just now</small>
</div>
<div class="text-truncate font-weight-bold">
<span class="fa fa-exclamation text-danger"></span> Important message</div>
<div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
</div>
</a>
<a class="dropdown-item" href="#">
<div class="message">
<div class="py-3 mr-3 float-left">
<div class="avatar">
<img class="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
<span class="avatar-status badge-warning"></span>
</div>
</div>
<div>
<small class="text-muted">John Doe</small>
<small class="text-muted float-right mt-1">5 minutes ago</small>
</div>
<div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
<div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
</div>
</a>
<a class="dropdown-item" href="#">
<div class="message">
<div class="py-3 mr-3 float-left">
<div class="avatar">
<img class="img-avatar" src="img/avatars/5.jpg" alt="admin@bootstrapmaster.com">
<span class="avatar-status badge-danger"></span>
</div>
</div>
<div>
<small class="text-muted">John Doe</small>
<small class="text-muted float-right mt-1">1:52 PM</small>
</div>
<div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
<div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
</div>
</a>
<a class="dropdown-item" href="#">
<div class="message">
<div class="py-3 mr-3 float-left">
<div class="avatar">
<img class="img-avatar" src="img/avatars/4.jpg" alt="admin@bootstrapmaster.com">
<span class="avatar-status badge-info"></span>
</div>
</div>
<div>
<small class="text-muted">John Doe</small>
<small class="text-muted float-right mt-1">4:03 PM</small>
</div>
<div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
<div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
</div>
 </a>
<a class="dropdown-item text-center" href="#">
<strong>View all messages</strong>
</a>
</div>
</li>
<li class="nav-item dropdown">
<a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
<img class="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
</a>
<div class="dropdown-menu dropdown-menu-right">
<div class="dropdown-header text-center">
<strong>Account</strong>
</div>
<a class="dropdown-item" href="#">
<i class="fa fa-bell-o"></i> Updates
<span class="badge badge-info">42</span>
</a>
<a class="dropdown-item" href="#">
<i class="fa fa-envelope-o"></i> Messages
<span class="badge badge-success">42</span>
</a>
<a class="dropdown-item" href="#">
<i class="fa fa-tasks"></i> Tasks
<span class="badge badge-danger">42</span>
</a>
<a class="dropdown-item" href="#">
<i class="fa fa-comments"></i> Comments
<span class="badge badge-warning">42</span>
</a>
<div class="dropdown-header text-center">
<strong>Settings</strong>
</div>
<a class="dropdown-item" href="#">
<i class="fa fa-user"></i> Profile</a>
<a class="dropdown-item" href="#">
<i class="fa fa-wrench"></i> Settings</a>
<a class="dropdown-item" href="#">
<i class="fa fa-usd"></i> Payments
<span class="badge badge-dark">42</span>
</a>
<a class="dropdown-item" href="#">
<i class="fa fa-file"></i> Projects
<span class="badge badge-primary">42</span>
</a>
<div class="dropdown-divider"></div>
<a class="dropdown-item" href="#">
<i class="fa fa-shield"></i> Lock Account</a>
<a class="dropdown-item" href="#">
<i class="fa fa-lock"></i> Logout</a>
</div>
</li>
</ul>
<button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
<span class="navbar-toggler-icon"></span>
</button>
<button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
<span class="navbar-toggler-icon"></span>
</button>
</header>


  <div class="app-body">
    <div class="sidebar">
      <!-- Sidebar content here -->
      <nav class="sidebar-nav ps ps--active-y">
        <ul class="nav">
          <li class="nav-item">
            <i class="nav-icon icon-speedometer"></i>
            <a href="" class="nav-link active">
              Dashboard
              <span class="badge badge-info">NEW</span>
            </a>

          </li>

          <li class="nav-title">
              User Management
          </li>
          <li class="nav-title">
              Roles
          </li>
        </ul>
      </nav>


    </div>
    <main class="main">
      <!-- Main content here -->
      @yield("content")
    </main>
    <aside class="aside-menu">
      <!-- Aside menu content here -->
    </aside>
  </div>
  <footer class="app-footer">
    <!-- Footer content here -->
  </footer>



  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, Bootstrap, then CoreUI  -->
  <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.min.js"></script>

  <script text="text/javascript">
    @yield('script')
  </script>

</body>
</html>
